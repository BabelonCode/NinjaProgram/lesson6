﻿using _000_DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _009_Expressions
{
    public static class Dapper
    {
        static Dapper()
        {
            _mapper = new DataMapper();
        }

        private static readonly IDataMapper _mapper;

        public static IEnumerable<TModel> Execute<TModel>(string query) 
            where TModel : class, new()
        {
            Func<IDataRecord, TModel> convert = _mapper.GetOrCreateFunc<TModel>();

            var repo = new DataRepository(Connection.Default);
            foreach (var blog in repo.Execute("select * from Blog", convert))
                yield return blog;
        }
    }
}