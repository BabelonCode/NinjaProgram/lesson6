﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace _009_Expressions
{
    internal static class Cache
    {
        static Cache()
        {
            _cacheDic = new Dictionary<Type, dynamic>();
        }

        private static readonly Dictionary<Type, dynamic> _cacheDic;

        public static Func<TSource, TResalt> CompileAndCache<TSource, TResalt>(Expression<Func<TSource, TResalt>> expression)
        {
            var func = expression.Compile();
            _cacheDic[func.GetType()] = func;
            return func;
        }

        public static bool TyrGetFunc<TSource, TResalt>(out Func<TSource, TResalt> func)
        {
            var type = typeof(Func<TSource, TResalt>);
            if(_cacheDic.TryGetValue(type, out var cacheFunc))
            {
                func = (Func<TSource, TResalt>)cacheFunc;
                return true;
            }

            func = null;
            return false;
        }
    }
}
