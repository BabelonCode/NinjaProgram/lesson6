﻿using System;
using System.Data;

namespace _009_Expressions
{
    public class DataMapper : IDataMapper
    {
        static DataMapper()
        {
            _provider = new MapperProvider();
        }

        private static readonly MapperProvider _provider;

        public Func<IDataRecord, TResult> GetOrCreateFunc<TResult>() 
            where TResult : class, new()
        {
            if (Cache.TyrGetFunc<IDataRecord, TResult>(out var func))
                return func;

            var exp = _provider.CreateDataRecordExpression<TResult>();
            return Cache.CompileAndCache(exp);
        }
    }
}
