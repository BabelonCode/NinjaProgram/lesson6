﻿using System;
using System.Data;

namespace _009_Expressions
{
    public interface IDataMapper
    {
        Func<IDataRecord, TResult> GetOrCreateFunc<TResult>()
            where TResult : class, new();
    }
}
